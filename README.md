# Wolf Simulator


## Introduction

"Be The Wolf" is a wolf simulation project developed using Unreal Engine and C++. It aims to provide an immersive experience of controlling a virtual wolf through various terrains with intuitive controls and realistic animations.

## Key Features

 - Intuitive Controls: Navigate the virtual wolf seamlessly through the environment.
 - Responsive Movement Mechanics: Smooth and responsive movement for an immersive experience.
 - Terrain-Adaptive Animations: Animation adjustments based on terrain slope using inverse kinematics and line tracing to prevent mesh overlap or crossing.
 - Detailed Terrain Response: Realistic adjustments to the wolf’s poses and animations based on specific terrain features.

## Tech Stack

 - Development: C++
 - Engine: Unreal Engine

## Installation

To open the source code:

1. Download and Install [Git](https://git-scm.com/) and [Git LFS](https://git-lfs.com/)

2. Download this repository with:
```
git clone https://gitlab.com/aman-batra-dev/be-the-wolf
cd be-the-wolf
git lfs intall
git lfs pull
```

3. Open Unreal Engine

4. Open project folder into Unreal Engine

5. Voila! playaround as you wish

## Credits

Made By:
[Aman Batra](https://gitlab.com/aman-batra-dev)
