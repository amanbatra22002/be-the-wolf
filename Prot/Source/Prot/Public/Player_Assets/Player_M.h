// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Materials/MaterialInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Player_M.generated.h"

UCLASS()
class PROT_API APlayer_M : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayer_M();
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		UCameraComponent* gcamera;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		USpringArmComponent* CameraBroom;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category="Custom_HUD",Meta = (BlueprintProtected = "true"))
		TSubclassOf<class UUserWidget> hudclass;
	UPROPERTY()
		class USkeletalMeshComponent* MainBody;
	UPROPERTY()
	    class UUserWidget* MyWid;
	UPROPERTY(BlueprintReadOnly,Category="Player_Controller")
		class APlayerController* MainController;
	UPROPERTY(EditAnywhere,Category="Materials")
		UMaterialInterface* Dash_Body;
	UPROPERTY(EditAnywhere,Category="Materials")
		UMaterialInterface* Original_Material;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		FRotator MovementRotation;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		bool IsWalking;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		float BaseDistance;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		float DistanceBack;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		float DistanceFront;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION(BlueprintCallable,Category = "Custom_Input_Functions")
		void TurnX(float value);
	UFUNCTION(BlueprintCallable,Category = "Custom_Input_Functions")
		void TurnY(float value);
	UFUNCTION(BlueprintCallable,Category = "Custom_Input_Functions")
		void MoveForward(float value);
	UFUNCTION(BlueprintCallable,Category = "Custom_Input_Functions")
		void MoveRight(float value);
	UFUNCTION(BlueprintCallable,Category = "Custom_Input_Functions")
		void Jump_Custom();
	UFUNCTION(BlueprintCallable,Category = "Custom_Input_Functions")
		void DashInitiate();
	UFUNCTION()
		void DashCompleted();
	UFUNCTION()
		void ToggleSprint();
	UFUNCTION(BlueprintCallable, Category = "IK_Functions")
		float IK_Foot_Tracer(FName Socket,float UPZVector);
	UFUNCTION()
		void Foot_Trace_Init();
	
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
