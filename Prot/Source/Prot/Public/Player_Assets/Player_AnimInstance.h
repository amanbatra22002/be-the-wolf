// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/Actor.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Player_Assets/Player_M.h"
#include "Player_AnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class PROT_API UPlayer_AnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	UPlayer_AnimInstance();
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		float movementSpeed;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		float direction;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		bool IsJumping;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		bool IsWalking;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		AActor* owner;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		APlayer_M* Wolf;
	UPROPERTY(EditAnywhere,Category="Montage")
		UAnimMontage* Jump_Montage;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		float DistanceBack;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		float DistanceFront;

protected:
	UFUNCTION(BlueprintCallable,Category="Custom_Animation_Functions")
		float calDirection(const FVector& velocity,const FRotator& rot);
	UFUNCTION()
		void Jump_Task();
	UFUNCTION()
		void DistanceFrontAndBack();
	virtual void NativeUpdateAnimation(float seconds) override;
	
};
