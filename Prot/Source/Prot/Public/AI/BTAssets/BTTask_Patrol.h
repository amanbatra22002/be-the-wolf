// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "AI/AICharacterBase.h"
#include "AI/AIControllerBase.h"
#include "AI/PatrolPointsGen.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BTTask_Patrol.generated.h"

/**
 * 
 */
UCLASS()
class PROT_API UBTTask_Patrol : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	public:
		UBTTask_Patrol();
		virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp,uint8* NodeMemory) override;
		TArray<FVector> Locs;
		uint8 index=0;
};
