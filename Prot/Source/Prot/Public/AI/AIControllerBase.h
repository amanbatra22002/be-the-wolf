// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Prot/Public/AI/AICharacterBase.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIControllerBase.generated.h"

/**
 * 
 */
UCLASS()
class PROT_API AAIControllerBase : public AAIController
{
	GENERATED_BODY()
	public:
		AAIControllerBase();
		virtual void OnPossess(APawn* InPawn) override;
		UPROPERTY(transient)
			class UBehaviorTreeComponent* BTC;
		UPROPERTY(transient)
			class UBlackboardComponent* BBC;
		
		//keys
		uint8 lol;
	
};
