// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BillboardComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SplineComponent.h"
#include "PatrolPointsGen.generated.h"

UCLASS()
class PROT_API APatrolPointsGen : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APatrolPointsGen();
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Patrol Components")
		class UBillboardComponent* billboard;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Patrol Components")
		class UArrowComponent* forwardArrow;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Patrol Components")
		class USplineComponent* spline;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	TArray<FVector> GetLocOfAllPoints();
	int NumOfPatPoints();

};
