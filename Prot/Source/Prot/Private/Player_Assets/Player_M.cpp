// Fill out your copyright notice in the Description page of Project Settings.


#include "Player_Assets/Player_M.h"

APlayer_M::APlayer_M()
{
	PrimaryActorTick.bCanEverTick = true;
	CameraBroom = CreateDefaultSubobject<USpringArmComponent>(TEXT("BBroom"));
	MainBody = GetMesh();
	CameraBroom->SetupAttachment(RootComponent);
	CameraBroom->SetRelativeLocation(FVector(0.0f,0.0f,50.0f));
	CameraBroom->TargetArmLength = 300.0f;
	CameraBroom->bUsePawnControlRotation = true;
	gcamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MMcamera"));
	gcamera->SetupAttachment(CameraBroom,USpringArmComponent::SocketName);
	gcamera->SetRelativeLocation(FVector(0.0f,100.0f,0.0f));
	gcamera->bUsePawnControlRotation = false;
	Dash_Body = CreateDefaultSubobject<UMaterialInterface>(TEXT("Dash_Mat"));
	Original_Material = CreateDefaultSubobject<UMaterialInterface>(TEXT("Main_Mat"));
	BaseDistance = 54.5f;
}

void APlayer_M::BeginPlay()
{
	Super::BeginPlay();
	MovementRotation = FRotator(0.0f,0.0f,0.0f);
	MainController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	if(hudclass!=NULL)
	{
		MyWid = CreateWidget<UUserWidget>(GetWorld(),hudclass);
		MyWid->AddToViewport();
	}
}
float APlayer_M::IK_Foot_Tracer(FName Socket,float UPZVector)
{
	FHitResult OutHit;
	TArray<AActor*> ignore_Actors;
	FVector Start = FVector(MainBody->GetSocketLocation(Socket).X,MainBody->GetSocketLocation(Socket).Y,GetActorLocation().Z);
	FVector End = FVector(Start.X,Start.Y,Start.Z+UPZVector);
	UKismetSystemLibrary::LineTraceSingle(GetWorld(),Start,End,TraceTypeQuery1,false,ignore_Actors,EDrawDebugTrace::Type::ForOneFrame,OutHit,true);
	if(OutHit.bBlockingHit)
	{
		float Distance = (OutHit.Location - Start).Z;
		return Distance;
	}
	return 0.0f;
}
void APlayer_M::Foot_Trace_Init()
{
	DistanceBack = (float)FMath::RoundToInt(FMath::Abs(IK_Foot_Tracer(FName("Wolf_BackTracer_Socket"),-150.0f))-BaseDistance);
	DistanceFront = (float)FMath::RoundToInt(FMath::Abs(IK_Foot_Tracer(FName("Wolf_FrontTracer_Socket"),-150.0f))-BaseDistance);
	UKismetSystemLibrary::PrintString(GetWorld(),FString("DB: ")+FString::SanitizeFloat(DistanceBack)+FString(",DF: ")+FString::SanitizeFloat(DistanceFront));
	
}
void APlayer_M::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Foot_Trace_Init();
}

void APlayer_M::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("TurnX",this,&APlayer_M::TurnX);
	PlayerInputComponent->BindAxis("TurnY",this,&APlayer_M::TurnY);
	PlayerInputComponent->BindAxis("MoveForward",this,&APlayer_M::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight",this,&APlayer_M::MoveRight);
	PlayerInputComponent->BindAction("Jump",IE_Pressed,this,&APlayer_M::Jump_Custom);
	PlayerInputComponent->BindAction("Jump",IE_Released,this,&APlayer_M::StopJumping);
	PlayerInputComponent->BindAction("Dash",IE_Pressed,this,&APlayer_M::DashInitiate);
	PlayerInputComponent->BindAction("ToggleSprint",IE_Pressed,this,&APlayer_M::ToggleSprint);
	
}
void APlayer_M::TurnX(float value)
{
	AddControllerYawInput(value);
}
void APlayer_M::TurnY(float value)
{
	AddControllerPitchInput(value);
}
void APlayer_M::MoveForward(float value)
{
	MovementRotation.Yaw = FMath::RInterpTo(MovementRotation,Controller->GetControlRotation(),UGameplayStatics::GetWorldDeltaSeconds(GetWorld()),3.0f).Yaw;
	FVector dir = FRotationMatrix(FRotator(0,MovementRotation.Yaw,0)).GetScaledAxis(EAxis::X);
	AddMovementInput(dir,value);
		
}
void APlayer_M::MoveRight(float value)
{
	MovementRotation.Yaw = FMath::RInterpTo(MovementRotation,Controller->GetControlRotation(),UGameplayStatics::GetWorldDeltaSeconds(GetWorld()),3.0f).Yaw;
	FVector rightVector = FRotationMatrix(FRotator(0,MovementRotation.Yaw,0)).GetScaledAxis(EAxis::Y);
	FVector forwardVector = FRotationMatrix(FRotator(0,GetActorRotation().Yaw,0)).GetScaledAxis(EAxis::X);
	rightVector *= value;
	forwardVector +=rightVector;
	forwardVector = forwardVector.GetSafeNormal();
	AddMovementInput(forwardVector,FMath::Abs(value));
}
void APlayer_M::DashInitiate()
{
	if(!GetVelocity().IsNearlyZero())
	{
		float launchSpeed = 10000;
		MainBody->SetMaterial(0,Dash_Body);
		DisableInput(MainController);
		UGameplayStatics::SetGlobalTimeDilation(GetWorld(),0.15f);
		FVector launchVelocity = GetVelocity().GetSafeNormal(0.0001f) * launchSpeed;
		LaunchCharacter(launchVelocity,false,false);
		FTimerHandle timerHandle;
		GetWorldTimerManager().SetTimer(timerHandle,this,&APlayer_M::DashCompleted,0.23f,false);

	}
}
void APlayer_M::DashCompleted()
{
	EnableInput(MainController);
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(),1.0f);
	MainBody->SetMaterial(0,Original_Material);
}
void APlayer_M::Jump_Custom()
{
	if(!GetVelocity().IsNearlyZero()&&!IsWalking)
		Jump();
}
void APlayer_M::ToggleSprint()
{
	if(IsWalking)
	{
		GetCharacterMovement()->MaxWalkSpeed = 600.0f;
		IsWalking = false;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = 150.0f;
		IsWalking = true;
	}
}