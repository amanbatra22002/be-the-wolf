// Fill out your copyright notice in the Description page of Project Settings.

#include "Player_Assets/Player_AnimInstance.h"

UPlayer_AnimInstance::UPlayer_AnimInstance()
{
    Jump_Montage = CreateDefaultSubobject<UAnimMontage>(TEXT("Jump_Montage"));
}
void UPlayer_AnimInstance::NativeUpdateAnimation(float seconds)
{
    Super::NativeUpdateAnimation(seconds);
    owner = GetOwningActor();
    if(owner!=nullptr)
    {
        FVector tempVector = owner->GetVelocity();
        tempVector = FVector(tempVector.X,tempVector.Y,0.0f);
        movementSpeed = tempVector.Size();
        direction = calDirection(tempVector,owner->GetActorRotation());
        Wolf = Cast<APlayer_M>(owner);
        if(Wolf!=nullptr)
        {
            Jump_Task();
            DistanceFrontAndBack();
        }
    }
}
void UPlayer_AnimInstance::Jump_Task()
{
    IsWalking = Wolf->IsWalking;
    IsJumping = Wolf->GetCharacterMovement()->IsFalling();
    if(IsJumping)
        if(!Montage_IsPlaying(Jump_Montage))
            Montage_Play(Jump_Montage,1.0f);
}
void UPlayer_AnimInstance::DistanceFrontAndBack()
{
    if(Wolf->DistanceBack>=0)
        DistanceBack = Wolf->DistanceBack; //Wolf->DistanceBack;
    else
        DistanceBack = 0.0f;

    if(Wolf->DistanceFront>=0)
        DistanceFront = Wolf->DistanceFront;//Wolf->DistanceFront;
    else
        DistanceFront = 0.0f;
}
float UPlayer_AnimInstance::calDirection(const FVector& velocity,const FRotator& rot)
{
    if(!velocity.IsNearlyZero())
    {
        FVector forwardVector = FRotationMatrix(FRotator(0,rot.Yaw,0)).GetScaledAxis(EAxis::X);
        FVector rightVector = FRotationMatrix(FRotator(0,rot.Yaw,0)).GetScaledAxis(EAxis::Y);
        FVector NormalizedSpeed = velocity.GetSafeNormal2D();
        float FCosTheta = FVector::DotProduct(forwardVector,NormalizedSpeed);
        float FCosDegree = FMath::RadiansToDegrees(FMath::Acos(FCosTheta));
        float RCosTheta = FVector::DotProduct(rightVector,NormalizedSpeed);
        if(RCosTheta<0)
        {
            FCosDegree *=-1;
        }
        return FCosDegree;
    }
    return 0.f;
}