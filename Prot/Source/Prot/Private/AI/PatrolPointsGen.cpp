// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/PatrolPointsGen.h"

// Sets default values
APatrolPointsGen::APatrolPointsGen()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	billboard = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
	billboard->SetupAttachment(GetRootComponent());
	forwardArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Forward Arrow"));
	forwardArrow->SetupAttachment(billboard);
	spline = CreateDefaultSubobject<USplineComponent>(TEXT("Spline"));
	spline->SetupAttachment(billboard);
}

// Called when the game starts or when spawned
void APatrolPointsGen::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APatrolPointsGen::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

TArray<FVector> APatrolPointsGen::GetLocOfAllPoints()
{
	TArray<FVector> locs;
	for(int i=0;i<spline->GetNumberOfSplinePoints();i++)
	{
		locs.Add(spline->GetLocationAtSplinePoint(i,ESplineCoordinateSpace::World));
	}
	return locs;
}

int APatrolPointsGen::NumOfPatPoints()
{
	return spline->GetNumberOfSplinePoints();
}



