// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/BTAssets/BTTask_Patrol.h"

UBTTask_Patrol::UBTTask_Patrol()
{
    bCreateNodeInstance = true;
    NodeName = "AI_Patrol";
}

EBTNodeResult::Type UBTTask_Patrol::ExecuteTask(UBehaviorTreeComponent& OwnerComp,uint8* NodeMemory)
{
    UBlackboardComponent* bbc = OwnerComp.GetBlackboardComponent();
    AAIController* aicontroller = OwnerComp.GetAIOwner();
    if(bbc==nullptr||aicontroller==nullptr)
    {
        return EBTNodeResult::Failed;
    }
    AAICharacterBase* aichar = Cast<AAICharacterBase>(aicontroller->GetPawn());
    if(aichar==nullptr)
    {
        return EBTNodeResult::Failed;
    }
    APatrolPointsGen* patPoints = Cast<APatrolPointsGen>(aichar->patpoints);
    if(patPoints==nullptr)
    {
        return EBTNodeResult::Failed;
    }
    index = bbc->GetValueAsInt("Next_Loc_Index");
    Locs = patPoints->GetLocOfAllPoints();
    bbc->SetValueAsVector("Next_Pat_Location",Locs[index]);
    if(index<Locs.Num()-1)
    {
        index++;
        bbc->SetValueAsInt("Next_Loc_Index",index);
        return EBTNodeResult::Succeeded;
    }
    else
    {
        index = 0;
        bbc->SetValueAsInt("Next_Loc_Index",index);
        return EBTNodeResult::Succeeded;
    }
}