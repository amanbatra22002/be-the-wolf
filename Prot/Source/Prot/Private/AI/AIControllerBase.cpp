// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/AIControllerBase.h"

AAIControllerBase::AAIControllerBase()
{
    BBC = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoard Component"));
    BTC = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Tree Component"));
}

void AAIControllerBase::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);
    AAICharacterBase* ch = Cast<AAICharacterBase>(InPawn);
    if(ch!=nullptr&&ch->treeAsset!=nullptr)
    {
        BBC->InitializeBlackboard(*ch->treeAsset->BlackboardAsset);
        BTC->StartTree(*ch->treeAsset);
    }
}

